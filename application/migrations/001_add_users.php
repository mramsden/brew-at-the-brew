<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_Users extends CI_Migration
{
	public function up()
	{
		$this->dbforge->add_field('id');
		$this->dbforge->add_field(array(
			'username' => array(
				'type' => 'VARCHAR',
				'constraint' => '50'
			),
			'password' => array(
				'type' => 'VARCHAR',
				'constraint' => 128
			),
			'salt' => array(
				'type' => 'VARCHAR',
				'constraint' => 128
			),
			'firstname' => array(
				'type' => 'VARCHAR',
				'constraint' => 128
			),
			'lastname' => array(
				'type' => 'VARCHAR',
				'constraint' => 128
			)
		));
		$this->dbforge->create_table('users');
	}

	public function down()
	{
		$this->dbforge->drop_table('users');
	}
}
