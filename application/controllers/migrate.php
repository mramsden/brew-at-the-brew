<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migrate extends CI_Controller
{
	public function index()
	{
		if (!$this->input->is_cli_request()) {
			show_404("migrate");
			exit;
		}
		
		$this->load->library('migration');
		if (!$this->migration->current()) {
			show_error($this->migration->error_string());
		}
	}
}
