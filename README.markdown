Brew @ The Brew
===============

Introduction
------------

A simple Codeigniter application created by Marcus Ramsden from Smile Machine.
It is designed to solve that headache of who likes tea and how. Please feel
free to fork this application if you want to take a look or have a play around
with it.

License
-------

This application is released under a do whatever you want with it license, so
long as it is does not involve aiding super villans and thwarting super heros.

